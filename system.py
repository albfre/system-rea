#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import urllib2
import xml.etree.cElementTree as ElementTree
import datetime

def valid_date(date_str):
    try:
        datetime.datetime.strptime(date_str, '%Y-%m-%d')
        return True
    except ValueError:
        return False

def get_map(date_str):
    xml = ElementTree.parse(date_str).getroot()
    artiklar = xml.findall('artikel')
    dictionary = {}
    for artikel in artiklar:
        nr = int(artikel.find('nr').text)
        namn = artikel.find('Namn').text
        pris = float(artikel.find('Prisinklmoms').text)
        saljstart = artikel.find('Saljstart').text
        alkoholhalt = artikel.find('Alkoholhalt').text
        dictionary[nr] = (namn, pris, saljstart, alkoholhalt)
    return dictionary

def diff_maps(map1, map2):
    decreased = []
    for nr in map2:
        if not nr in map1: continue
        p1 = map1[nr][1]
        p2 = map2[nr][1]
        if p2 < p1:
            decreased.append((nr, p2 / p1))
    decreased.sort(key=lambda x: x[1])
    return decreased


today_str = str(datetime.date.today())
if not os.path.isfile(today_str):
    print "Laddar ner data"
    response = urllib2.urlopen('https://www.systembolaget.se/api/assortment/products/xml')
    xml_str = response.read()
    print "Klar"
    with open(today_str, 'w') as f:
        f.write(xml_str)

today_map = get_map(today_str)

previous_files = [f for f in os.listdir('.') if os.path.isfile(f) and valid_date(f) and f != today_str]
previous_files.sort()
assert len(previous_files) > 0
previous_date_str = previous_files[-1]
assert previous_date_str < today_str, 'Previous date: ' + previous_date_str + ', today: ' + today_str

print 'J�mf�r ' + today_str + ' med ' + previous_date_str
previous_date_map = get_map(previous_date_str)

decreased = diff_maps(previous_date_map, today_map)
decreased_strs = []
for x in decreased:
    nr = x[0]
    name = today_map[nr][0]
    p1 = previous_date_map[nr][1]
    p2 = today_map[nr][1]
    saljstart = today_map[nr][2]
    alkoholhalt = today_map[nr][3]
    percent = (1.0 - p2 / p1) * 100.0
    s1 = name.encode('latin-1') + ', ' + alkoholhalt + ' (Nr ' + str(nr) + ') ' + str(p1) + ' kr (' + previous_date_str + ') -> ' + str(p2) + ' kr (' + today_str + '). S�ljstart: ' + saljstart
    s2 = 'Priss�nkning {:.3}%'.format(percent)
    print s2 + ". " + s1
    decreased_strs.append(s2 + ". " + s1)

if len(decreased_strs) > 0:
    with open('rea_' + today_str + '.txt', 'w') as f:
        f.writelines(decreased_strs)
        f.close()
